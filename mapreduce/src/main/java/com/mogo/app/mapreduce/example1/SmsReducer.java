package com.mogo.app.mapreduce.example1;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by mohangoyal on 8/21/16.
 */
public class SmsReducer extends Reducer<Text, Text, Text, Text> {

    //Variables to aid the join process
    private String customerName, deliveryReport;

    /**
     * Map to store Delivery Codes and Messages
     * Key being the status code and vale being the status message
     *
     */
    private static Map<String, String> DeliveryCodesMap = new HashMap<String, String>();


    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        super.setup(context);
        loadDeliveryStatusCodes();
    }


    @Override
    public void reduce(final Text key, final Iterable<Text> values, final Context context)
            throws IOException, InterruptedException {

        Iterator<Text> v = values.iterator();
        while(v.hasNext()) {
            String currValue = v.next().toString();
            String valueSplitted[] = currValue.split("~");
            if (valueSplitted[0].equals("CD")) {
                customerName = valueSplitted[1].trim();
            } else if (valueSplitted[0].equals("DR")) {
                //getting the delivery code and using the same to obtain the Message
                deliveryReport = DeliveryCodesMap.get(valueSplitted[1].trim());
            }

        }
        //pump final output to file
        if (customerName != null && deliveryReport != null) {
            context.write(new Text(customerName), new Text(deliveryReport));
        } else if (customerName == null)
            context.write(new Text("customerName"), new Text(deliveryReport));
        else if (deliveryReport == null)
            context.write(new Text(customerName), new Text("deliveryReport"));

    }


    //To load the Delivery Codes and Messages into a hash map
    private void loadDeliveryStatusCodes() {
        String strRead;
        try {
            //read file from Distributed Cache
            BufferedReader reader = new BufferedReader(new FileReader("input/example1/DeliveryStatusCodes.txt"));
            while ((strRead = reader.readLine()) != null) {
                String splitarray[] = strRead.split(",");
                //parse record and load into HahMap
                DeliveryCodesMap.put(splitarray[0].trim(), splitarray[1].trim());

            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
