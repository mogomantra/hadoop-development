package com.mogo.app.mapreduce.example1;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;

import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.MultipleInputs;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

/**
 * Created by mohangoyal on 8/21/16.
 */
public class SmsDriver extends Configured implements Tool {
    public int run(String[] args) throws Exception {

        Job job = Job.getInstance(getConf(), "SMS Reports");

        //setting key value types for mapper and reducer outputs
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);

        //specifying the custom reducer class
        job.setReducerClass(SmsReducer.class);

        //Specifying the input directories(@ runtime) and Mappers independently for inputs from multiple sources
        MultipleInputs.addInputPath(job, new Path(args[0]), TextInputFormat.class, UserFileMapper.class);
        MultipleInputs.addInputPath(job, new Path(args[1]), TextInputFormat.class, DeliveryFileMapper.class);

        //Specifying the output directory @ runtime
        FileOutputFormat.setOutputPath(job, new Path(args[2]));

        int retValue = job.waitForCompletion(true) ? 0 : 1;
        return retValue;
    }

    public static void main(String[] args) throws Exception {


        int res = ToolRunner.run(new Configuration(), new SmsDriver(),
                args);
        System.exit(res);
    }
}