package com.mogo.app.mapreduce.example1;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/**
 * Created by mohangoyal on 8/21/16.
 */
public class UserFileMapper extends Mapper<LongWritable, Text, Text, Text>
{
    //variables to process Consumer Details
    private String cellNumber,customerName,fileTag="CD~";

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        super.setup(context);
    }

    /**
     *
     * map method that process ConsumerDetails.txt and frames the initial key value pairs
     *  Key(Text) – mobile number
     *  Value(Text) – An identifier to indicate the source of input(using ‘CD’ for the customer details file) + Customer Name
     *
     * @param key
     * @param value
     * @param context
     * @throws IOException
     * @throws InterruptedException
     */
    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        //taking one line/record at a time and parsing them into key value pairs
        String line = value.toString();
        String splitarray[] = line.split(",");
        cellNumber = splitarray[0].trim();
        customerName = splitarray[1].trim();
        context.write(new Text(cellNumber), new Text(fileTag+customerName));

    }
}

