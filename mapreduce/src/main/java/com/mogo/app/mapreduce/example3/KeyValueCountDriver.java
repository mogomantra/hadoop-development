package com.mogo.app.mapreduce.example3;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

/**
 * Created by mohangoyal on 8/28/16.
 */
public class KeyValueCountDriver extends Configured implements Tool {

    /**
     * Construct a Configured.
     */
    public KeyValueCountDriver() {
        super();
    }

    /**
     * Construct a Configured.
     *
     * @param conf
     */
    public KeyValueCountDriver(Configuration conf) {
        super(conf);
    }

    @Override
    public void setConf(Configuration conf) {
        super.setConf(conf);
    }

    @Override
    public Configuration getConf() {
        return super.getConf();
    }

    /**
     * Execute the command with the given arguments.
     *
     * @param args command specific arguments.
     * @return exit code.
     * @throws Exception
     */
    public int run(String[] args) throws Exception {

        if (args.length != 2) {
            System.err.printf("Usage: %s needs two arguments, input and output " +
                    "files\n", getClass().getSimpleName());
            return -1;
        }

        Configuration conf = this.getConf();
        Job job = Job.getInstance(conf, "KeyValuePair Job");

        job.setJarByClass(KeyValueCountDriver.class);
        job.setMapperClass(KeyValueCountMapper.class);
        job.setCombinerClass(KeyValueCountReducer.class);
        job.setReducerClass(KeyValueCountReducer.class);


        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(DoubleWritable.class);
        job.setOutputFormatClass(TextOutputFormat.class);

        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));

        FileSystem fs = FileSystem.get(conf);
        boolean isDirExist = fs.isDirectory(new Path(args[1]));
        if(isDirExist) {
            fs.delete(new Path(args[1]), true);
        }

        int returnValue = job.waitForCompletion(false) ? 0: 1;

        if(job.isSuccessful()) {
            System.out.println("Job was successful");
        } else if(!job.isSuccessful()) {
            System.out.println("Job was not successful");
        }


        return returnValue;
    }


    /**
     *
     * @param args
     * @throws Exception
     */
    public static void main(final String[] args) throws Exception {
        final int returnCode = ToolRunner.run(new Configuration(), new KeyValueCountDriver(), args);
        System.exit(returnCode);
    }
}
