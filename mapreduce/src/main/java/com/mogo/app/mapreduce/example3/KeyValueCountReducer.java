package com.mogo.app.mapreduce.example3;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

/**
 * Created by mohangoyal on 8/28/16.
 */
public class KeyValueCountReducer extends Reducer<Text, DoubleWritable, Text, DoubleWritable> {
    private DoubleWritable valueDouble = new DoubleWritable(0);
    public KeyValueCountReducer() {
        super();
    }

    /**
     * Called once at the start of the task.
     *
     * @param context
     */
    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        super.setup(context);
    }

    /**
     * This method is called once for each key. Most applications will define
     * their reduce class by overriding this method. The default implementation
     * is an identity function.
     *
     * @param key
     * @param values
     * @param context
     */
    @Override
    protected void reduce(Text key, Iterable<DoubleWritable> values, Context context) throws IOException, InterruptedException {
        //super.reduce(key, values, context);
        int count = 0;
        double sum = 0;
        for(DoubleWritable value : values) {
            sum += value.get();
            count++;
        }

        double average = sum/count;
        valueDouble.set(average);
        context.write(key, valueDouble);

    }

    /**
     * Called once at the end of the task.
     *
     * @param context
     */
    @Override
    protected void cleanup(Context context) throws IOException, InterruptedException {
        super.cleanup(context);
    }

    /**
     * Advanced application writers can use the
     * {@link #run(Context)} method to
     * control how the reduce task works.
     *
     * @param context
     */
    @Override
    public void run(Context context) throws IOException, InterruptedException {
        super.run(context);
    }
}
