package com.mogo.app.mapreduce.example3;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/**
 * Created by mohangoyal on 8/28/16.
 */
public class KeyValueCountMapper extends Mapper<LongWritable, Text, Text, DoubleWritable>{

    private DoubleWritable valueDouble = new DoubleWritable(0);
    private Text keyText = new Text();

    /**
     * Called once at the beginning of the task.
     *
     * @param context
     */
    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        super.setup(context);
    }

    /**
     * Called once for each key/value pair in the input split. Most applications
     * should override this, but the default is the identity function.
     *
     * @param key
     * @param value
     * @param context
     */
    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

        //Get the line and break this into key and value
        String line = value.toString();
        String[] strArray = line.split(",");

        //Save the each text
        keyText.set(strArray[0]);
        double number = 0.0;
        if(strArray.length > 1) {
            try {
                number = Double.parseDouble(strArray[1]);
            } catch (NumberFormatException e) {
                e.printStackTrace();
                number = 0.0;
            }
        }else {
            number = 0.0;
        }
        valueDouble.set(number);
        context.write(keyText, valueDouble);
    }

    /**
     * Called once at the end of the task.
     *
     * @param context
     */
    @Override
    protected void cleanup(Context context) throws IOException, InterruptedException {
        super.cleanup(context);
    }

    /**
     * Expert users can override this method for more complete control over the
     * execution of the Mapper.
     *
     * @param context
     * @throws IOException
     */
    @Override
    public void run(Context context) throws IOException, InterruptedException {
        super.run(context);
    }
}
